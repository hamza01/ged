var folder = companyhome.childByNamePath('Sites/'+args.site+'/documentLibrary/Valides')
if (folder == undefined || !folder.isContainer) { 
    status.code = 404; 
    status.message = "Site " + args.site + " not found."; 
    status.redirect = true; 
} 
model.folder = folder;
model.site = args.site;
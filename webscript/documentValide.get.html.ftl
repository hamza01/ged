<html>

<head>
    <title>${folder.displayPath}/${folder.name}</title>
</head>

<body> Folder: ${folder.displayPath}/${folder.name} <br>
    <table>
        <#list folder.children as child>
            <tr>
                <td>
                    <p>${child.name}</p>
                </td>
            </tr>
        </#list>
    </table>
</body>
</html>